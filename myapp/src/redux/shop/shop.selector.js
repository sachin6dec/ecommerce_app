import {createSelector} from 'reselect';

// const COLLECTION_ID_MAP = {
// 	hats:1,
// 	sneakers:2,
// 	jackets:3,
// 	women:4,
// 	men:5
// }

const selectShop = (state) => state.shop;


export const selectCollections = createSelector(
		[selectShop],
		(shop)=>shop.collestions
);

export const selectCollectionForPreview = createSelector(
		[selectCollections],
		collections => Object.keys(collections).map(key => collections[key])
	);



export const selectCollection = collectionUrlParam =>
	createSelector(
		[selectCollections],		
		collections=>collections[collectionUrlParam]
	)


////collestions=>collestions.find(collection => collection.id === COLLECTION_ID_MAP[collectionUrlParam])