import React from 'react';
import { Route } from 'react-router-dom';
import {connect} from 'react-redux';
import {updateCollections} from '../../redux/shop/shop.actions';
import CollectionOverview from '../../component/collections-overview/collection-overview.component';
import CollectionPage from '../collection/collection.component';
import 	{firestore, convertCollectionsSnapShotToMap} from '../../firebase/firebase.utils';

class ShopPage extends React.Component{
		unsubscribeFromSnapShot = null;

		componentDidMount(){
			const {updateCollections} = this.props;
			const collectionRef = firestore.collection('collections');
			collectionRef.onSnapshot(async snapshot => {
				const collectionMap = convertCollectionsSnapShotToMap(snapshot);
				updateCollections(collectionMap);
			});
		}
		render(){
			const {match} = this.props;
			return(
					<div className='shop-page'>
						<Route exact path={`${match.path}`} component={CollectionOverview} />
						<Route path= {`${match.path}/:collectionId`} component={CollectionPage} />
					</div>
		)}}

const mapDispatchToProps = dispatch =>({
	updateCollections : collectoionsMap => dispatch(updateCollections(collectoionsMap))
})
export default connect(null, mapDispatchToProps)(ShopPage);