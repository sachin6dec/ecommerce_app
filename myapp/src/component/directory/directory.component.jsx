import React from 'react';
import './directory.styles.scss';
import {connect} from 'react-redux';

import MenuItem from '../menu-item/menu-item.component';

import {createStructuredSelector} from 'reselect';

import {selectDirectorySections} from  '../../redux/directory/directory.selector';

const Directory = ({sections}) => {
	return (
			<div className='directory-menu'>
				{
					sections.map(({id,...section}) => (
						<MenuItem key={id} {...section}/>
					))
				}
			</div>
		)	
}

const mapStateToProps = createStructuredSelector({
	sections:selectDirectorySections
})
export default connect(mapStateToProps)(Directory);



