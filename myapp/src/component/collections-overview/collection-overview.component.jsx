import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import CollectionPreview from '../../component/collection-preview/collection-preview.component';
import {selectCollectionForPreview} from  '../../redux/shop/shop.selector';
import './collection-overview.styles.scss';

const CollectionOverview = ({collestions}) =>{

			return(
				<div className='shop-page'>
					{
							collestions.map(({id, ...otherCollectionProps }) => (
							<CollectionPreview key={id} {...otherCollectionProps} />
						))
						
					}
				</div>
			);
	}


const mapStateToProps = createStructuredSelector({
	collestions:selectCollectionForPreview
})

export default connect(mapStateToProps)(CollectionOverview);