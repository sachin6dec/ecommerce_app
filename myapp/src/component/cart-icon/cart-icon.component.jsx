import React from 'react';
import { connect } from 'react-redux';
import {toggleCartHidden} from '../../redux/cart/cart.action';
import {createStructuredSelector} from 'reselect';
import {selectCartItemsCount} from '../../redux/cart/cart.selectors';
import {ReactComponent as Shoppingicon} from '../../assets/shopping-bag.svg';
import './cart-icon.styles.scss';


const CartIcon = ({toggleCartHidden, itemCount}) =>(
	
		<div className = 'cart-icon' onClick={toggleCartHidden}>
			<Shoppingicon className='shopping-icon' />
			<span className='item-count'>{itemCount}</span>
		</div>
	
)
const mapDispatchToProps = (dispatch) => ({
	toggleCartHidden: () => dispatch(toggleCartHidden())
})

const mapStatesToProps = createStructuredSelector({	
	itemCount: selectCartItemsCount
})

export default connect(mapStatesToProps, mapDispatchToProps)(CartIcon);