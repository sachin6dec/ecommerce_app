import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAwXp5k6Q-pedwlJRLT8wBnzoOdQdXb5Mo",
    authDomain: "crwn-db-d0f44.firebaseapp.com",
    projectId: "crwn-db-d0f44",
    storageBucket: "crwn-db-d0f44.appspot.com",
    messagingSenderId: "786861210141",
    appId: "1:786861210141:web:1c9bb4a2d511b1c53564f4",
    measurementId: "G-CV4BGDG3P8"
  };
firebase.initializeApp(config);
  export const createUserProfileDocument = async(userAuth,additionalData)=>{
  	if(!userAuth) return;


  	const userRef = firestore.doc(`users/${userAuth.uid}`);

  	const snapShot = await userRef.get();

  	if(!snapShot.exists){
  		const {displayName, email} = userAuth;
  		const createAt = new Date();
  		try{
  			await userRef.set({
  				displayName,
  				email,
  				createAt,
  				...additionalData
  			})
  		}
  		catch(error){
  			console.log('error createing user', error.message);
  		}
  	}	
  	return userRef;
  }

  export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) =>{
      const collectionRef = firestore.collection(collectionKey);
      const batch = firestore.batch();
      objectsToAdd.forEach(obj=>{
        const newDocRef = collectionRef.doc();
        batch.set(newDocRef, obj);
      })
      return await batch.commit()
  }


  export const convertCollectionsSnapShotToMap = (collections) => {
      const transformedCollection = collections.docs.map( doc => {
              const { title, items } = doc.data();

              return {
                 routeName : encodeURI(title.toLowerCase()),
                 id: doc.id,
                 title,
                 items
              }
      })
     return  transformedCollection.reduce((accumulator, collection) => {
                accumulator[collection.title.toLowerCase()] = collection;
                return accumulator;
              },{});
  }



  

  export const auth = firebase.auth();
  export const firestore = firebase.firestore();

  const provider = new firebase.auth.GoogleAuthProvider();
  provider.setCustomParameters({prompt: 'select_account'});

  export const signInWithGoogle = () => auth.signInWithPopup(provider);

  export default firebase;