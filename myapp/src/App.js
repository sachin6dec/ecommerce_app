import React from 'react';
import HomePage from './pages/homepage/homepage.component';
import { Route, Switch, Redirect} from 'react-router-dom';
import ShopPage from './pages/shop/shop.component';
import Header from './component/header/header.component';
import CheckoutPage from './pages/checkout/checkout.component';
//import { auth, createUserProfileDocument, addCollectionAndDocuments } from './firebase/firebase.utils';
import { auth, createUserProfileDocument} from './firebase/firebase.utils';

//import {selectCollectionForPreview} from './redux/shop/shop.selector';

import { connect } from 'react-redux';
import {setCurrentUser} from './redux/user/user.action';

import SingInAndSignUpPage from './pages/sign-in-and-sign-up/sign-in-and-sign-up.component';

import {createStructuredSelector} from 'reselect';
import {selectCurrentUser} from './redux/user/user.selector';

class  App extends React.Component {

	unsubscribeFromAuth = null;
	componentDidMount(){
		//const {setCurrentUser, collectionsArray} = this.props;
		const {setCurrentUser} = this.props;
		this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth=>{
			if(userAuth){
				const userRef = await createUserProfileDocument(userAuth);
				userRef.onSnapshot(snapShot =>{
					setCurrentUser({
						currentUser:{
							id:snapShot.id,
							...snapShot.data()
						}
					});
				});
			}else{
				setCurrentUser(userAuth);
				// addCollectionAndDocuments('collections', 
				// 	collectionsArray.map(({title, items}) => ({title, items})));
			}
		});
	}

	componentWillUnmount(){
		this.unsubscribeFromAuth();
	}
	render(){	
		  return (
		      <div>      
		      	<Header /> 
		      	<Switch>    
		          <Route exact path='/' component={HomePage} />
		          <Route path='/shop' component={ShopPage} />
		          <Route exact path='/checkout' component={CheckoutPage} />
		          <Route exact path='/signin' 
		          		 render={
		          		 			() => this.props.currentUser ? 
		          		 			(<Redirect to='/' />) :
		          		 			(<SingInAndSignUpPage />)} 
		          	/>
		        </Switch>  
		      </div>
		  );
	}  
}

const mapStateToProps = createStructuredSelector({
	currentUser:selectCurrentUser,
	//collectionsArray:selectCollectionForPreview
})
const mapDispatchToProps = (dispatch) =>({
	setCurrentUser: user => dispatch(setCurrentUser(user))
})

export default connect (mapStateToProps, mapDispatchToProps)(App);
